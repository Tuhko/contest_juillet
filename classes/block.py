# -*- coding: utf-8 -*-
# !/usr/bin/python

import hashlib
import pandas as pd
import json
import os
import uuid


from classes.wallet import Wallet
from settings.settings import Settings


class Block:
    def __init__(self, block_size, base_hash, parent_hash,
                 transactions, id_transaction):
        self.hash = self.get_hash()
        file_exist = os.path.exists(f'{Settings.get_blocks_path()}'
                                    f'{self.hash}.json')
        if not file_exist:
            self.block_size = block_size
            self.transactions_dict = {}
            self.base_hash = base_hash
            self.parent_hash = parent_hash
            self.transactions = transactions
            self.id_transaction = id_transaction
            self.save()
        else:
            self.load(self.hash)
            self.block_size = block_size
            self.transactions = transactions

        if transactions is None:
            transactions = []
        self.base_hash = Settings.create_base_hash()
        self.taille = self.get_weight()

        self.parent_hash = parent_hash
        self.transactions = transactions

    @staticmethod
    def get_hash():
        """
        Génère un hash en fonction d'un string aléatoire
        :return: string
        """
        h = hashlib.sha256(Settings.create_base_hash().encode())
        h = h.hexdigest()
        # print(h)
        return h

    def verify_hash(self):
        """
        Vérifie que le hash corresponde bien.
        :return: Boolean
        """
        if self.get_hash() == self.hash:
            return True
        else:
            return False

    def add_transaction(self, emetteur, recepteur, montant):
        """
        :param emetteur: string
        :param recepteur: string
        :param montant: integer
        :param id: integer
        :return:
        """
        wallet_emitter = Wallet(emetteur)
        wallet_receptor = Wallet(recepteur)
        t = uuid.uuid1()
        self.transactions = str(t)
        datas = {
            self.transactions: {
                "wallet_emitter": emetteur,
                "wallet_receptor": recepteur,
                "montant": montant,
                "ID de la transaction": self.id_transaction
            },
        }

        test = wallet_emitter.send(datas, montant, 'emitter')

        # print(test)
        if not test:
            print('\n\t Echec de la transaction')

        else:

            wallet_receptor.send(datas, montant, 'receptor')
            self.transactions_dict.update(datas)
            print('\n\t Transaction réussie.')
            self.save()

    def get_transaction(self, transaction):
        return self.transactions[transaction]

    def get_weight(self):
        path = f'{Settings.get_blocks_path()}{self.hash}.json'
        return os.stat(path).st_size

    def save(self):
        data = {
            'name': self.hash,
            'base_hash': self.base_hash,
            'parent_hash': self.parent_hash,
            'transactions': self.transactions
        }

        file_path = f'{Settings.get_blocks_path()}{self.hash}.json'

        with open(file_path, 'w+') as outfile:
            str_ = json.dumps(data,
                              indent=4, sort_keys=True,
                              separators=(',', ': '), ensure_ascii=False)
            outfile.write(str_)

    def load(self, hash_target):
        with open(f"{Settings.get_blocks_path()}{hash_target}.json") as file:
            target_block_data = json.load(file)
        self.hash = target_block_data['name']
        self.base_hash = target_block_data['base_hash']
        self.parent_hash = target_block_data['parent_hash']
        self.transactions_dict = target_block_data['transactions']
        # pprint(target_block_data)
        return target_block_data

    def load_dataframe(self):
        df = pd.DataFrame(self.load(
            '5a510cc6b7519673b75920b9e3e00140'
            '7b42a124fcaaa0026432ca6f379230b3'),
            index=[0])
        return df
