
    Projet étudiant Contest Campus Academy



    Contexte du projet :


La technologie Blockchain est de plus en plus utilisée dans de nombreuses thématiques. Il est donc primordial de s’y intéresser en tant que développeur et de comprendre à minima les mécaniques de base. Vous devrez donc, à l’aide des indications que vous trouverez ci-dessous, réaliser une ébauche de blockchain fonctionnelle.


    Langage de programmation

La technologie blockchain est connue pour être gourmande en ressources, vous devrez donc utiliser le langage Python afin de réaliser ce projet. Celui-ci étant notamment réputé pour sa rapidité de réalisation de calculs, il est donc tout indiqué.

    Fonctionnement

Je n'ai pas réussi à développer toutes les fonctionnalités requises.

Je souhaitais demander à l'utilisateur (input()), dans le fichier main.py, quelles fonctionnalités il souhaitait utiliser, et lui demander ensuite, suivant sa réponse, d'indiquer les paramètres requis.

J'ai cherché à factoriser ce qui me semblait pertinent, et ai regroupé toutes mes variables dans le fichier settings.py. 

    Vérification (avec flake8)

Comme dit ci-dessus, je n'ai pas réussi à mettre au point toutes les fonctionnalités, mais j'ai testé flake8 sur les fichiers disponibles, cela m'a permis de corriger certaines erreurs et d'apprendre quelques bonnes pratiques par la même occasion (Je le réutiliserai tout le temps désormais !)