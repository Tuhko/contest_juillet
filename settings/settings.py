# -*- coding: utf-8 -*-
# !/usr/bin/python

import os
import string
import random


class Settings:
    def __init__(self):
        pass

    @staticmethod
    def get_max_token_number():
        return os.environ.get("MAX_TOKEN_NUMBER", '')

    @staticmethod
    def get_block_hash_test_1():
        return os.environ.get("B1_TEST_HASH",
                              '0f74b2b3fef8513d7426e8633b7d2'
                              '19c977cae11d14cd72b58e85d117e4f806f')

    @staticmethod
    def get_block_hash_test_2():
        return os.environ.get("B2_TEST_HASH",
                              '05d74a37ef6ea39c184ec507778cf6f1f'
                              'a6a5be1235743f1fbf8257bbbde9f23')

    @staticmethod
    def get_wallet_id_test_1():
        return os.environ.get("W1_TEST_ID",
                              '24e0d9a0-dfd6-11eb-9f02-a0999b1cc837')

    @staticmethod
    def get_wallet_id_test_2():
        return os.environ.get("W2_TEST_ID",
                              'dffdc302-dfee-11eb-b7ce-a0999b1cc837')

    @staticmethod
    def get_blocks_path():
        return os.environ.get("BLOCKS_PATH", 'content/blocks/')

    @staticmethod
    def get_wallets_path():
        return os.environ.get("WALLETS_PATH", 'content/wallets/')

    @staticmethod
    def create_base_hash():
        """
        Créé un string aléatoire qui servira de base au Hachage
         (dans get_hash())
        :return: string
        """
        rand_letters = string.ascii_letters
        rand = ''.join(random.choice(rand_letters) for i in range(10))
        # print(rand)
        return rand
